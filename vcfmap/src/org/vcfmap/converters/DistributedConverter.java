package org.vcfmap.converters;

import org.vcfmap.alignment.AlignmentManager;
import org.vcfmap.alignment.Assembly;

/**
 * 
 * @author steelea
 * 
 */
public class DistributedConverter extends Converter {

	/**
	 * 
	 * @param alnManager
	 * @param origRef
	 * @param newRef
	 */
	private DistributedConverter(AlignmentManager alnManager, Assembly origRef,
			Assembly newRef) {
		super(alnManager, origRef, newRef);
	}

	/**
	 * 
	 */
	public void convert(String vcfFile) {
	}

	/*
	 * public void convert(String vcfFile){
	 * 
	 * try{ FileReader freader = new FileReader(vcfFile); BufferedReader
	 * inputFile = new BufferedReader(freader);
	 * 
	 * String line = inputFile.readLine(); while (line != null){ if
	 * (line.trim().length() ==0) break;
	 * 
	 * String[] lsplit = line.split("\t");
	 * 
	 * //Discard indels and multiallelic if (lsplit[4].length() == 1 &&
	 * lsplit[3].length() == 1){ int key = Integer.parseInt(lsplit[1]); //Make
	 * sure alignment exists if (index.containsKey(key)){ //Make sure doesn't
	 * align to insertion String dest = index.get(key); if (!dest.equals("-1")){
	 * //All conditions met, convert this stuff String[] dsplit =
	 * dest.split(":"); lsplit[0] = dsplit[0]; lsplit[1] = dsplit[1]; char
	 * strand = dsplit[2].charAt(0);
	 * 
	 * //Get the reference allele int coord = Integer.parseInt(dsplit[1]);
	 * String ref_allele = "" + refScaffolds.get(dsplit[0]).charAt(coord-1);
	 * String convertedLine =
	 * StandardConverter.convertLineGenotype(lsplit,ref_allele, strand);
	 * System.out.println(convertedLine); } }
	 * 
	 * } line = inputFile.readLine(); } }catch(Exception e){
	 * System.err.println("Error parsing the vcf file: " + vcfFile);
	 * e.printStackTrace(); System.exit(1); }
	 * 
	 * }
	 */
}
