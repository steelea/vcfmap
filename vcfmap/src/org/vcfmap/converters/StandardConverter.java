package org.vcfmap.converters;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.HashMap;

import org.vcfmap.alignment.AlignmentManager;
import org.vcfmap.alignment.Assembly;
import org.vcfmap.misc.Utils;

/**
 * 
 * @author steelea
 *
 */
public class StandardConverter extends Converter {

	/**
	 * 
	 * @param alnManager
	 * @param newRef
	 * @param origRef
	 */
	public StandardConverter(AlignmentManager alnManager, Assembly newRef,
			Assembly origRef) {
		super(alnManager, newRef, origRef);
	}

	/**
	 * 
	 */
	public void convert(String vcfFile) {
		try {
			FileReader freader = new FileReader(vcfFile);
			BufferedReader inputFile = new BufferedReader(freader);

			String line = inputFile.readLine();

			/***** Convert the VCF Header *******/
			// Print out all header except contigs and reference
			while (line.startsWith("##")) {
				if (!line.startsWith("##contig")
						&& !line.startsWith("##reference"))
					System.out.println(line);
				line = inputFile.readLine();
			}

			/****** Print out new Header Contigs *******/
			System.out.println(reference.getVCFHeaderString());

			/****** Print out the line with VCF column labels *******/
			System.out.println(line);

			/***** Convert the Variant Entries ******/
			String curScaf = "";
			String destSeq = "";
			String destScaf = "";
			line = inputFile.readLine();
			HashMap<Integer, String> index = new HashMap<Integer, String>();
			while (line != null) {
				if (line.trim().length() == 0)
					break;

				String[] lsplit = line.split("\t");

				// Discard indels and multiallelic
				if (lsplit[4].length() == 1 && lsplit[3].length() == 1) {

					// Get Cigar mapping if not existant
					if (!lsplit[0].equals(curScaf)) {
						curScaf = lsplit[0];
						index = alnManager.getAlignmentsForSequence(curScaf);
						System.err.println("Processing scaffold: " + curScaf);
					}

					// Make sure alignment exists
					int key = Integer.parseInt(lsplit[1]);
					if (index.containsKey(key)) {
						// Make sure doesn't align to insertion
						String dest = index.get(key);

						// All conditions met, start conversion
						if (!dest.equals("-1")) {
							String[] dsplit = dest.split(":");
							lsplit[0] = dsplit[0]; // the new reference scaffold
							lsplit[1] = dsplit[1]; // the new reference position
							char strand = dsplit[2].charAt(0);

							// Load the destination sequence if different
							if (!destScaf.equals(lsplit[0])) {
								destScaf = lsplit[0];
								destSeq = reference.getSequence(destScaf);
							}

							// Get the reference allele
							int coord = Integer.parseInt(dsplit[1]);

							String refAllele = "" + destSeq.charAt(coord - 1);
							String convertedLine = convertLineGenotype(lsplit,
									refAllele, strand);
							if (convertedLine != null)
								System.out.println(convertedLine);
						}
					}
				}
				line = inputFile.readLine();

			}
			inputFile.readLine();
			freader.close();
		} catch (Exception e) {
			System.err.println("Error parsing the vcf file: " + vcfFile);
			e.printStackTrace();
			System.exit(1);
		}

	}

	/**
	 * 
	 * @param lsplit
	 * @param refAllele
	 * @param strand
	 * @return
	 */
	public static String convertLineGenotype(String[] lsplit, String refAllele,
			char strand) {
		// Check the forward strand
		if (strand == '+') {
			return convertForward(lsplit, refAllele);
		} else {
			return convertReverse(lsplit, refAllele);
		}
	}

	/**
	 * 
	 * @param lsplit
	 * @param refAllele
	 * @return
	 */
	public static String convertForward(String[] lsplit, String refAllele) {
		// Reference isn't equal in destination coordinates
		if (!lsplit[3].equals(refAllele)) {

			/****** FIXED DIFFERENCE SNP *******/
			if (lsplit[4].equals(".")) {
				lsplit[4] = lsplit[3];
				lsplit[3] = refAllele;
				for (int i = 9; i < lsplit.length; i++) {
					if (lsplit[i].contains("/")) {
						String[] ssplit = lsplit[i].split(":");
						if (ssplit[0].equals("0/0"))
							ssplit[0] = "1/1";

						lsplit[i] = Utils.join(ssplit, ":");
					}
				}
			}

			/****** Multiallelic SNP ******/
			else if (!lsplit[4].equals(refAllele)) {
				lsplit[4] = lsplit[3] + "," + lsplit[4];
				lsplit[3] = refAllele;
				for (int i = 9; i < lsplit.length; i++) {
					if (lsplit[i].contains("/")) {
						String[] ssplit = lsplit[i].split(":");
						if (ssplit[0].equals("0/0"))
							ssplit[0] = "1/1";
						else if (ssplit[0].equals("0/1"))
							ssplit[0] = "1/2";
						else if (ssplit[0].equals("1/1"))
							ssplit[0] = "2/2";
						else if (ssplit[0].equals("1/0"))
							ssplit[0] = "2/1";

						lsplit[i] = Utils.join(ssplit, ":");
					}
				}
			}

			/****** REF AND ALTERNATE FLIPPED SNP *****/
			else if (lsplit[4].equals(refAllele)) {
				lsplit[4] = lsplit[3];
				lsplit[3] = refAllele;
				for (int i = 9; i < lsplit.length; i++) {
					if (lsplit[i].contains("/")) {
						String[] ssplit = lsplit[i].split(":");
						if (ssplit[0].equals("0/1"))
							ssplit[0] = "1/0";
						else if (ssplit[0].equals("1/1"))
							ssplit[0] = "0/0";
						else if (ssplit[0].equals("1/0"))
							ssplit[0] = "1/0";
						else if (ssplit[0].equals("0/0"))
							ssplit[0] = "1/1";

						lsplit[i] = Utils.join(ssplit, ":");
					}
				}
			}
		}

		return Utils.join(lsplit, "\t");
	}

	/**
	 * 
	 * @param lsplit
	 * @param refAllele
	 * @return
	 */
	public static String convertReverse(String[] lsplit, String refAllele) {

		// If ref isn't equal to reverse complement
		if (!lsplit[3].equals(Utils.complement(refAllele))) {

			/***** Fixed Difference SNP *****/
			if (lsplit[4].equals(".")) {
				lsplit[4] = Utils.complement(lsplit[3]);
				lsplit[3] = refAllele;

				for (int i = 9; i < lsplit.length; i++) {
					if (lsplit[i].contains("/")) {
						String[] ssplit = lsplit[i].split(":");
						if (ssplit[0].equals("0/0"))
							ssplit[0] = "1/1";

						lsplit[i] = Utils.join(ssplit, ":");
					}
				}
			}

			/***** Multiallelic SNP ******/
			else if (!Utils.complement(lsplit[4]).equals(refAllele)) {
				lsplit[4] = Utils.complement(lsplit[3]) + ","
						+ Utils.complement(lsplit[4]);
				lsplit[3] = refAllele;

				// Update Phasing data
				for (int i = 9; i < lsplit.length; i++) {
					if (lsplit[i].contains("/")) {
						String[] ssplit = lsplit[i].split(":");
						if (ssplit[0].equals("0/0"))
							ssplit[0] = "1/1";
						else if (ssplit[0].equals("0/1"))
							ssplit[0] = "1/2";
						else if (ssplit[0].equals("1/0"))
							ssplit[0] = "2/1";
						else if (ssplit[0].equals("1/1"))
							ssplit[0] = "2/2";

						lsplit[i] = Utils.join(ssplit, ":");
					}
				}
			}

			/****** REF AND ALTERNATE FLIPPPED SNP *****/
			else if (lsplit[4].equals(Utils.complement(refAllele))) {
				lsplit[4] = Utils.complement(lsplit[3]);
				lsplit[3] = refAllele;
				for (int i = 9; i < lsplit.length; i++) {
					if (lsplit[i].contains("/")) {
						String[] ssplit = lsplit[i].split(":");
						if (ssplit[0].equals("0/0"))
							ssplit[0] = "1/1";
						else if (ssplit[0].equals("0/1"))
							ssplit[0] = "1/0";
						else if (ssplit[0].equals("1/1"))
							ssplit[0] = "0/0";
						else if (ssplit[0].equals("1/0"))
							ssplit[0] = "1/0";

						lsplit[i] = Utils.join(ssplit, ":");
					}
				}
			}
		} else {
			lsplit[3] = refAllele;
			if (!lsplit[4].equals("."))
				lsplit[4] = Utils.complement(lsplit[4]);
		}
		return Utils.join(lsplit, "\t");
	}
}
