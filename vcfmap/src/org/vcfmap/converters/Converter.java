package org.vcfmap.converters;

import org.vcfmap.alignment.AlignmentManager;
import org.vcfmap.alignment.Assembly;

/**
 * 
 * @author steelea
 * 
 */
public abstract class Converter {
	protected Assembly reference;
	protected Assembly query;
	protected AlignmentManager alnManager;

	/**
	 * 
	 * @param alnManager
	 * @param reference
	 * @param query
	 */
	public Converter(AlignmentManager alnManager, Assembly reference,
			Assembly query) {
		this.reference = reference;
		this.query = query;
		this.alnManager = alnManager;
	}

	/**
	 * 
	 * @param vcfFile
	 */
	public abstract void convert(String vcfFile);
}
