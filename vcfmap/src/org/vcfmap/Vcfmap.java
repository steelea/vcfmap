package org.vcfmap;

import org.apache.commons.cli.*;
import org.vcfmap.alignment.AlignmentManager;
import org.vcfmap.alignment.Assembly;
import org.vcfmap.converters.StandardConverter;

public class Vcfmap {

	/**
	 * @param args
	 */
	private static String alignmentFile = "";
	private static String vcfFile = "";
	private static String originalReference = "";
	private static String newReference = "";
	private static boolean isCigar = false;
	private static int threads = 1;
	private static String mafOrigLabel = "";
	private static String mafNewLabel = "";

	/**
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		parseArgs(args);
		checkRequiredInputs();

		// Load the assemblies
		Assembly newRef = Assembly.importAssembly(newReference);
		Assembly origRef = Assembly.importAssembly(originalReference);

		// Create the appropriate alignment manager
		AlignmentManager alnManager;
		if (isCigar)
			alnManager = AlignmentManager.importFromCigar(alignmentFile,
					newRef, origRef);
		else
			alnManager = AlignmentManager.importFromMaf(alignmentFile,
					mafNewLabel, mafOrigLabel, newRef, origRef);

		// Create a converter and do the conversion
		StandardConverter sc = new StandardConverter(alnManager, newRef,
				origRef);
		sc.convert(vcfFile);

	}

	/**
	 * 
	 */
	public static void checkRequiredInputs() {
		if (alignmentFile.equals("")) {
			System.err
					.println("An alignment parameters is a required (-maf or -cigar)");
			System.exit(1);
		} else if (vcfFile.equals("")) {
			System.err.println("-vcf is a required parameter");
			System.exit(1);
		} else if (originalReference.equals("")) {
			System.err.println("-origref is a required parameter");
			System.exit(1);
		} else if (newReference.equals("")) {
			System.err.println("-newref is a required parameter");
			System.exit(1);
		}
	}

	/**
	 * 
	 * @param args
	 */
	public static void parseArgs(String[] args) {
		// Set the boolean options
		Options options = new Options();
		Option help = new Option("help", "print this message");
		Option version = new Option("version",
				"print the version information and exit [Currently Not Implemented]");
		// Option mf = new Option("makeflow",
		// "Output a makeflow [Currently Not Implemented]");
		// Option threads = new Option("threads",
		// "Number of threads (Default=1) [Currently Not Implemented]");
		// options.addOption(help);
		options.addOption(help);
		options.addOption(version);
		// options.addOption(mf);
		// options.addOption(threads);

		// Set the vcf input options
		Option vcf = OptionBuilder.withArgName("vcf").hasArg()
				.withDescription("VCF file to convert").create("vcf");
		options.addOption(vcf);

		// Reference input parameters
		Option origref = OptionBuilder.withArgName("origref").hasArg()
				.withDescription("The current reference of the VCF file")
				.create("origref");
		Option newref = OptionBuilder.hasArg()
				.withDescription("The destination reference file")
				.withArgName("newref").create("newref");
		options.addOption(origref);
		options.addOption(newref);

		// Alignment input parameters
		Option maf = OptionBuilder.withArgName("maf").hasArg()
				.withDescription("MAF alignment file").create("maf");
		// Option cigar =
		// OptionBuilder.withArgName("cigar").hasArg().withDescription("Cigar Alignment; used when running makeflow").create("cigar");
		Option mafOrig = OptionBuilder
				.withArgName("mafOrig")
				.hasArg()
				.withDescription(
						"The label of the original reference in the MAF file")
				.create("mafOrig");
		Option mafNew = OptionBuilder
				.withArgName("mafNew")
				.hasArg()
				.withDescription(
						"The label of the new reference in the MAF file")
				.create("mafNew");
		options.addOption(maf);
		options.addOption(mafOrig);
		options.addOption(mafNew);
		// options.addOption(cigar);

		// Parse the input
		CommandLineParser parser = new BasicParser();

		try {
			CommandLine cmd = parser.parse(options, args);

			// If help flag was included
			if (cmd.hasOption("help") || cmd.hasOption("h")) {
				HelpFormatter formatter = new HelpFormatter();
				formatter.printHelp("vcfmap.jar", options);
				System.exit(0);
			}

			// Get the input parameters
			if (cmd.hasOption("vcf")) {
				vcfFile = cmd.getOptionValue("vcf");
			}
			if (cmd.hasOption("maf")) {
				alignmentFile = cmd.getOptionValue("alignment");
			}
			if (cmd.hasOption("mafOrig")) {
				mafOrigLabel = cmd.getOptionValue("mafOrig");
			}
			if (cmd.hasOption("mafNew")) {
				mafNewLabel = cmd.getOptionValue("mafNew");
			}
			if (cmd.hasOption("origref")) {
				originalReference = cmd.getOptionValue("origref");
			}
			if (cmd.hasOption("newref")) {
				newReference = cmd.getOptionValue("newref");
			}
			if (cmd.hasOption("cigar")) {
				alignmentFile = cmd.getOptionValue("alignment");
				isCigar = true;
			}

		} catch (ParseException exp) {
			System.out.println("Unexpected exception:" + exp.getMessage());
		}

	}
}
