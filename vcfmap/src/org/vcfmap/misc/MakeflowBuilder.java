package org.vcfmap.misc;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.StringTokenizer;


/**
 * 
 * @author steelea
 *
 */
public class MakeflowBuilder {

	/**
	 * 
	 * @param args
	 */
	public static void main(String args[]) {
		// String vcfFile = args[0];
		// String cigarFile = args[1];
		// String refFile = args[2];

		String vcfFile = "meru_test.vcf";
		String cigarFile = "meru_all.cig";
		String refFile = "Anopheles-gambiae-PEST_CHROMOSOMES_AgamP3.fa";

		// Parse the ref file
		String vcfHeader = parseRefFile(refFile);

		// Create new vcf file
		createNewVcfFile(vcfFile, vcfHeader);

		// split the cigar file
		splitCigarFile(cigarFile);

		// Split the vcf file
		splitVcfFile(vcfFile);
	}

	/**
	 * 
	 * @param vcfFile
	 * @param vcfHeader
	 */
	public static void createNewVcfFile(String vcfFile, String vcfHeader) {
		System.err.println("Building Convert VCF file header");
		String filename = vcfFile.replace(".vcf", "_cnvrt.vcf");
		try {
			FileReader freader = new FileReader(vcfFile);
			BufferedReader br = new BufferedReader(freader);
			FileWriter fw = new FileWriter(filename);
			PrintWriter pw = new PrintWriter(fw);

			String line = br.readLine();
			// Copy the first part of the VCF HEader
			while (line != null) {
				if (line.startsWith("##contig"))
					break;
				pw.println(line);
				line = br.readLine();
			}
			// Replace the contigs in the header
			while (line.startsWith("##contig")
					|| line.startsWith("##reference"))
				line = br.readLine();
			pw.print(vcfHeader);

			// Copy remainig part of the header
			while (line.startsWith("#")) {
				pw.println(line);
				line = br.readLine();
			}
			// Close up shop
			br.close();
			freader.close();
			fw.close();
			pw.close();

		} catch (Exception e) {
			e.printStackTrace();
			System.exit(1);
		}
	}

	/**
	 * 
	 * @param filename
	 * @return
	 */
	public static String parseRefFile(String filename) {
		System.err.println("Parsing the Reference File");
		HashMap<String, String> scaffolds = new HashMap<String, String>();
		String vcfHeader = "";
		try {
			FileReader freader = new FileReader(filename);
			BufferedReader inputFile = new BufferedReader(freader);

			StringBuffer sb = null;
			String header = "";
			int index = -1;
			String line = inputFile.readLine();
			while (line != null) {
				if (line.startsWith(">")) {
					System.err.println("\t" + line);
					if (index != -1)
						scaffolds.put(header, sb.toString());
					StringTokenizer st = new StringTokenizer(line, " ", false);
					header = st.nextToken().replace(">", "");
					index++;
					sb = new StringBuffer();
				} else {
					line = line.toUpperCase().replace("\n", "");
					sb.append(line);
				}
				line = inputFile.readLine();
			}
			// Add the last scaffold
			scaffolds.put(header, sb.toString());
			inputFile.close();
			freader.close();

			// Put all the contigs together
			Object[] keySet = scaffolds.keySet().toArray();
			for (int i = 0; i < keySet.length; i++) {
				vcfHeader += "##contig=<ID=" + (String) keySet[i] + ",length="
						+ scaffolds.get(keySet[i]).length() + ">\n";
			}
			vcfHeader += "##reference=file://" + filename + "\n";

		} catch (Exception e) {
			System.err.println("Error reading from file: " + filename);
			e.printStackTrace();
			System.exit(1);
		}
		return vcfHeader;

	}

	/**
	 * 
	 * @param filename
	 */
	public static void splitCigarFile(String filename) {
		System.err.println("Splitting the Cigar File by contig");
		String curScaf = "";
		String line = "";
		String lsplit[];
		try {
			FileReader freader = new FileReader(filename);
			BufferedReader br = new BufferedReader(freader);
			line = br.readLine();
			HashMap<String, String> scaf_cigs = new HashMap<String, String>();

			while (line != null) {
				lsplit = line.trim().split(" ")[2].split("\\.");
				lsplit[1] = lsplit[1].replace("chr", "");
				lsplit[0] = "";
				curScaf = Utils.cleanJoin(lsplit, ".");
				// cur_scaf =
				// line.trim().split(" ")[2].split("\\.")[1].replace("chr","");
				String curVal = scaf_cigs.get(curScaf);
				if (curVal != null)
					scaf_cigs.put(curScaf, Utils.join(curVal, line, "\n"));
				else
					scaf_cigs.put(curScaf, line);

				line = br.readLine();
			}

			br.close();
			freader.close();

			// Write out the cigar contents
			FileWriter fw;
			PrintWriter pw;

			for (String key : scaf_cigs.keySet()) {
				fw = new FileWriter(key + ".idx.tmp");
				pw = new PrintWriter(fw);
				pw.write(scaf_cigs.get(key));
				pw.close();
				fw.close();
			}

		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(line);
			System.exit(1);
		}
	}

	/**
	 * 
	 * @param filename
	 */
	public static void splitVcfFile(String filename) {
		System.err.println("Splitting the VCF file by contig");
		try {
			FileReader freader = new FileReader(filename);
			BufferedReader br = new BufferedReader(freader);
			String line = br.readLine();
			String cur_scaf = "";
			FileWriter outfile = null;
			PrintWriter pw = null;
			StringBuilder sb = new StringBuilder();
			int count = 0;

			while (line != null) {
				line = line.trim();
				if (!line.startsWith("#")) {
					String[] lsplit = line.split("\t");
					if (!lsplit[0].equals(cur_scaf)) {
						if (sb.length() > 0) {
							// Write out all data for the scaffold
							System.err.println(cur_scaf);
							pw.println(sb.toString());
							outfile.close();
							pw.close();
						}
						// Get the new scaffold
						cur_scaf = lsplit[0];
						sb = new StringBuilder();
						count = 0;
						outfile = new FileWriter(cur_scaf + ".vcf.tmp");
						pw = new PrintWriter(outfile);

					} else if (count == 100000) {
						// System.err.println("Early printing");
						pw.print(sb.toString());
						sb = new StringBuilder();
						count = 0;
					}

					sb.append(line + "\n");
					count += 1;
				}
				line = br.readLine();
			}

			// Write out the last one
			System.err.println(cur_scaf);
			pw.println(sb.toString());
			outfile.close();
			pw.close();

		} catch (Exception e) {
			e.printStackTrace();
			System.exit(1);
		}
	}

}
