package org.vcfmap.misc;

/**
 * 
 * @author steelea
 *
 */
public class Utils {

	/**
	 * 
	 * @param base
	 * @return
	 */
	public static char complement(char base) {
		base = Character.toUpperCase(base);
		if (base == 'A')
			return 'T';
		if (base == 'T')
			return 'A';
		if (base == 'G')
			return 'C';
		if (base == 'C')
			return 'G';
		return 'N';
	}

	/**
	 * 
	 * @param base
	 * @return
	 */
	public static String complement(String base) {
		return "" + complement(base.charAt(0));
	}

	/**
	 * 
	 * @param input
	 * @param delimeter
	 * @return
	 */
	public static String join(String[] input, String delimeter) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < input.length - 1; i++) {
			sb.append(input[i]);
			sb.append(delimeter);
		}
		sb.append(input[input.length - 1]);
		return sb.toString();
	}

	/**
	 * 
	 * @param s1
	 * @param s2
	 * @param delimeter
	 * @return
	 */
	public static String join(String s1, String s2, String delimeter) {
		StringBuilder sb = new StringBuilder();
		sb.append(s1);
		sb.append(delimeter);
		sb.append(s2);
		return sb.toString();
	}

	/**
	 * 
	 * @param input
	 * @param delimeter
	 * @return
	 */
	public static String cleanJoin(String[] input, String delimeter) {
		// Omits empty input strings
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < input.length - 1; i++) {
			if (!input[i].equals("")) {
				sb.append(input[i]);
				sb.append(delimeter);
			}
		}
		if (!input[input.length - 1].equals(""))
			sb.append(input[input.length - 1]);
		return sb.toString();
	}
}
