package org.vcfmap.alignment;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.HashMap;

import org.vcfmap.misc.Utils;

/**
 * 
 * @author steelea
 *
 */
public class FixedDifferenceFinder {

	private Assembly reference;
	private Assembly query;
	private AlignmentManager alnManager;

	/**
	 * 
	 * @param cigarFile
	 * @param refFile
	 * @param queryFile
	 */
	public FixedDifferenceFinder(String cigarFile, String refFile,
			String queryFile) {
		reference = Assembly.importAssembly(refFile);
		query = Assembly.importAssembly(queryFile);
		alnManager = AlignmentManager.importFromCigar(cigarFile, reference,
				query);
	}

	/**
	 * 
	 * @param vcfFile
	 */
	public void processVCF(String vcfFile) {
		try {
			FileReader freader = new FileReader(vcfFile);
			BufferedReader inputFile = new BufferedReader(freader);

			String line = inputFile.readLine();

			/***** Convert the VCF Header *******/
			// Print out all header except contigs and reference
			while (line.startsWith("##")) {
				if (!line.startsWith("##contig")
						&& !line.startsWith("##reference"))
					System.out.println(line);
				line = inputFile.readLine();
			}

			// Print out new contigs
			for (String seq : query.getSequenceList())
				System.out.println("##contig=<ID=" + seq + ",length="
						+ query.getSequenceLength(seq) + ">");

			// Print new reference and last lines
			System.out.println("##reference=file://" + reference.getFilename());
			System.out.println(line);

			/***** Convert the Variant Entries ******/
			String curScaf = "";
			String curSeq = "";
			line = inputFile.readLine();
			HashMap<Integer, String> index = new HashMap<Integer, String>();
			while (line != null) {
				if (line.trim().length() == 0)
					break;

				String[] lsplit = line.split("\t");

				// Discard indels and multiallelic
				if (lsplit[4].length() == 1 && lsplit[3].length() == 1) {

					// Get Cigar mapping if not existant
					if (!lsplit[0].equals(curScaf)) {
						curScaf = lsplit[0];
						curSeq = reference.getSequence(curScaf);
						index = alnManager.getAlignmentsForSequence(curScaf);
					}

					// Make sure alignment exists
					int key = Integer.parseInt(lsplit[1]);
					if (index.containsKey(key)) {
						// Make sure doesn't align to insertion
						String dest = index.get(key);
						// All conditions met, start conversion
						if (!dest.equals("-1")) {
							String[] dsplit = dest.split(":");
							lsplit[0] = dsplit[0];
							lsplit[1] = dsplit[1];
							char strand = dsplit[2].charAt(0);

							// Get the reference allele
							int coord = Integer.parseInt(dsplit[1]);
							String refAllele = "" + curSeq.charAt(coord - 1);
							String convertedLine = findFixedDifferences(lsplit,
									refAllele, strand);
							if (convertedLine != null)
								System.out.println(convertedLine);
						}
					}

				}
				line = inputFile.readLine();
			}
			inputFile.close();
			freader.close();
		} catch (Exception e) {
			System.err.println("Error parsing the vcf file: " + vcfFile);
			e.printStackTrace();
			System.exit(1);
		}

	}

	/**
	 * 
	 * @param lsplit
	 * @param ref_allele
	 * @param strand
	 * @return
	 */
	private String findFixedDifferences(String[] lsplit, String ref_allele,
			char strand) {
		// Check the forward strand
		if (strand == '+') {
			if (!lsplit[3].equals(ref_allele)) {
				if (lsplit[4].equals(".")) {
					lsplit[4] = lsplit[3];
					lsplit[3] = ref_allele;
					// Update genotype information
					for (int i = 9; i < lsplit.length; i++) {
						String[] ssplit = lsplit[i].split(":");
						if (ssplit[0].equals("0/0")) {
							ssplit[0] = "1/1";
							lsplit[i] = Utils.join(ssplit, ":");
						}
					}
					return Utils.join(lsplit, "\t");
				}
			}
		} else {
			if (!lsplit[3].equals(Utils.complement(ref_allele))) {
				if (lsplit[4].equals(".")) {
					lsplit[4] = Utils.complement(lsplit[3]);
					lsplit[3] = ref_allele;

					// Updating genotype information
					for (int i = 9; i < lsplit.length; i++) {
						String[] ssplit = lsplit[i].split(":");
						if (ssplit[0].equals("0/0")) {
							ssplit[0] = "1/1";
							lsplit[i] = Utils.join(ssplit, ":");
						}
					}
					return Utils.join(lsplit, "\t");
				}
			}
		}

		return null;
	}

}
