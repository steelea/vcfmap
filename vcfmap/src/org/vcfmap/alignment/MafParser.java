package org.vcfmap.alignment;

import java.io.BufferedReader;
import java.io.FileReader;


/**
 * 
 * @author steelea
 *
 */
public class MafParser {

	// TODO: Add error checking somewhere to make sure seq in (A,G,C,T,N)

	/**
	 * 
	 * @param ref
	 * @param query
	 * @param filename
	 * @return
	 */
	public String getCigar(String ref, String query, String filename) {
		String cigString = "";
		try {
			FileReader freader = new FileReader(filename);
			BufferedReader br = new BufferedReader(freader);

			String line = br.readLine();
			String aln = "";
			while (line != null) {
				if (line.startsWith("a")) {
					if (!aln.equals("")) {
						cigString += parseSingleAlignment(aln, ref, query);
						aln = "";
					}
					aln += line;
				}
			}
			cigString += parseSingleAlignment(aln, ref, query);

			br.close();
			freader.close();
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("Error parsing maf file.\n ....Exiting");
			System.exit(1);
		}
		return cigString;
	}

	/**
	 * 
	 * @param align
	 * @param ref
	 * @param query
	 * @return
	 */
	private String parseSingleAlignment(String align, String ref, String query) {
		String[] lines = align.split("\n");
		String refSeq = "";
		String querySeq = "";
		String score = "";

		// Check to make sure ref and query sequences exists and are are same
		// length
		if (querySeq.length() == 0 || refSeq.length() == 0
				|| score.equals("NA") || querySeq.length() != refSeq.length())
			return "";

		// TODO: Add error checking somewhere to make sure seq in (A,G,C,T,N)
		// Generate the uncompressed cigar string
		String tempString = "";
		for (int i = 0; i < refSeq.length(); i++) {
			// If gap in both sequences then skip
			if (refSeq.charAt(i) == '-' && querySeq.charAt(i) == '-')
				;
			else if (querySeq.charAt(i) != '-') {
				if (refSeq.charAt(i) == '-')
					tempString += 'I';
				else
					tempString += 'M';
			} else if (refSeq.charAt(i) != '-') {
				if (querySeq.charAt(i) != '-')
					tempString += 'M';
				else
					tempString += 'D';
			}
		}

		// Compress tempString into a cigar string
		char current = tempString.charAt(0);
		int count = 1;
		String cigarString = "";
		for (int i = 1; i < tempString.length(); ++i) {
			if (tempString.charAt(i) == current)
				count += 1;
			else {
				cigarString += count + current + ",";
				current = tempString.charAt(i);
				count = 1;
			}
		}
		return cigarString.substring(0, tempString.length() - 1);
	}

}
