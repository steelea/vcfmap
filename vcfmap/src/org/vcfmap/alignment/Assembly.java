package org.vcfmap.alignment;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.HashMap;
import java.util.Set;
import java.util.StringTokenizer;


/**
 * 
 * @author steelea
 *
 */
public class Assembly {

	private HashMap<String, String> sequences;
	private String filename;

	/**
	 * 
	 */
	private Assembly() {
		sequences = new HashMap<String, String>();
	}

	/**
	 * 
	 * @param header
	 * @param sequence
	 */
	public void addSequence(String header, String sequence) {
		sequences.put(header, sequence);
	}
	
	
	/**
	 * 
	 * @param header
	 * @return
	 */
	public String getSequence(String header) {
		if (sequences.containsKey(header))
			return sequences.get(header);
		return null;
	}

	/**
	 * 
	 * @param header
	 * @return
	 */
	public int getSequenceLength(String header) {
		if (sequences.containsKey(header))
			return sequences.get(header).length();
		return -1;
	}

	/**
	 * 
	 * @return
	 */
	public Set<String> getSequenceList() {
		return sequences.keySet();
	}

	/**
	 * 
	 * @param filename
	 */
	private void setFilename(String filename) {
		this.filename = filename;
	}

	/**
	 * 
	 * @return
	 */
	public String getFilename() {
		return filename;
	}

	/**
	 * 
	 * @return
	 */
	public String getVCFHeaderString() {
		StringBuffer headerString = new StringBuffer();
		for (String s : sequences.keySet()) {
			headerString.append("##contig=<ID=" + s + ",length="
					+ sequences.get(s).length() + ">\n");
		}
		headerString.append("##reference=file://" + filename);
		return headerString.toString();
	}

	/**
	 * 
	 * @param filename
	 * @return
	 */
	public static Assembly importAssembly(String filename) {
		Assembly assem = new Assembly();
		assem.setFilename(filename);

		try {
			FileReader freader = new FileReader(filename);
			BufferedReader inputFile = new BufferedReader(freader);
			System.err.println("Reading Reference: " + filename);

			StringBuffer sb = null;
			String header = "";
			int index = -1;
			String line = inputFile.readLine();
			while (line != null) {
				if (line.startsWith(">")) {
					System.err.println("\t" + line);
					if (index != -1)
						assem.addSequence(header, sb.toString());
					StringTokenizer st = new StringTokenizer(line, " ", false);
					header = st.nextToken().replace(">", "");
					index++;
					sb = new StringBuffer();
				} else {
					line = line.toUpperCase().replace("\n", "");
					sb.append(line);
				}
				line = inputFile.readLine();
			}
			assem.addSequence(header, sb.toString());

			inputFile.close();
			freader.close();
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(0);
		}

		return assem;
	}
}
