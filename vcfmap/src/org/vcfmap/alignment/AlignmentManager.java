package org.vcfmap.alignment;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;

import org.vcfmap.misc.Utils;

/**
 * 
 * @author steelea
 *
 */
public class AlignmentManager {

	private HashMap<String, String> cigarStrings;
	private Assembly newRef;
	private Assembly origRef;

	/**
	 * 
	 */
	private AlignmentManager() {
		cigarStrings = new HashMap<String, String>();
	}

	/**
	 * 
	 * @param contig
	 * @param cigar
	 */
	private void addCigarAlignment(String contig, String cigar) {
		if (cigarStrings.containsKey(contig))
			cigarStrings.put(contig, cigarStrings.get(contig) + "\n" + cigar);
		else
			cigarStrings.put(contig, cigar);
	}

	/**
	 * 
	 * @param filename
	 * @param newLabel
	 * @param origLabel
	 * @param newRef
	 * @param origRef
	 * @return
	 */
	public static AlignmentManager importFromMaf(String filename,
			String newLabel, String origLabel, Assembly newRef, Assembly origRef) {
		AlignmentManager alnManager = new AlignmentManager();
		alnManager.newRef = newRef;
		alnManager.origRef = origRef;

		String cigString = "";
		String contig = "";
		try {
			FileReader freader = new FileReader(filename);
			BufferedReader br = new BufferedReader(freader);

			String line = br.readLine();
			String aln = "";
			while (line != null) {
				if (line.startsWith("a")) {
					if (!aln.equals("")) {
						cigString = AlignmentManager.parseSingleMafAlignment(
								aln, origLabel, newLabel);
						contig = cigString.split(" ")[2].split(".")[1];
						alnManager.addCigarAlignment(contig, cigString);
						aln = "";
					}
					aln += line;
				}
			}
			cigString += AlignmentManager.parseSingleMafAlignment(aln,
					origLabel, newLabel);

			br.close();
			freader.close();
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("Error parsing maf file.\n ....Exiting");
			System.exit(1);
		}

		return alnManager;
	}

	/**
	 * 
	 * @param align
	 * @param ref
	 * @param query
	 * @return
	 */
	private static String parseSingleMafAlignment(String align, String ref,
			String query) {
		String[] lines = align.split("\n");
		String refSeq = "";
		String querySeq = "";
		String score = "";

		// Check to make sure ref and query sequence exists and are same length
		if (querySeq.length() == 0 || refSeq.length() == 0
				|| score.equals("NA") || querySeq.length() != refSeq.length())
			return "";

		String tempString = "";
		for (int i = 0; i < refSeq.length(); ++i) {
			// If gap in both sequences then skip
			if (refSeq.charAt(i) == '-' && querySeq.charAt(i) == '-')
				;
			else if (querySeq.charAt(i) != '-') {
				if (refSeq.charAt(i) == '-')
					tempString += 'I';
				else
					tempString += 'M';
			} else if (refSeq.charAt(i) != '-') {
				if (querySeq.charAt(i) != '-')
					tempString += 'M';
				else
					tempString += 'D';
			}
		}

		// Compress tempString into a cigar string
		char current = tempString.charAt(0);
		int count = 1;
		String cigarString = "";
		for (int i = 1; i < tempString.length(); ++i) {
			if (tempString.charAt(i) == current)
				count += 1;
			else {
				cigarString += count + current + ",";
				current = tempString.charAt(i);
				count = 1;
			}
		}
		// Return, trimming off trailing comma
		return cigarString.substring(0, cigarString.length() - 1);
	}

	/**
	 * 
	 * @param filename
	 * @param newRef
	 * @param origRef
	 * @return
	 */
	public static AlignmentManager importFromCigar(String filename,
			Assembly newRef, Assembly origRef) {
		AlignmentManager alnManager = new AlignmentManager();
		alnManager.newRef = newRef;
		alnManager.origRef = origRef;

		try {
			FileReader freader = new FileReader(filename);
			BufferedReader inputFile = new BufferedReader(freader);
			String line = inputFile.readLine();
			System.err.print("Reading alignment file: " + filename + "...");
			while (line != null) {
				String[] lSplit = line.split("\\s");
				String[] sSplit = lSplit[2].split("\\.");
				sSplit[1] = sSplit[1].replace("chr", "");
				sSplit[0] = "";

				String sContig = Utils.cleanJoin(sSplit, ".");
				alnManager.addCigarAlignment(sContig, line);

				line = inputFile.readLine();
			}

			inputFile.close();
			freader.close();

		} catch (Exception e) {
			System.err.println("Error!");
			e.printStackTrace();
			System.exit(2);
		}
		System.err.println("Done!");
		return alnManager;
	}

	/**
	 * 
	 * @param contig
	 * @return
	 */
	public HashMap<Integer, String> getAlignmentsForSequence(String contig) {
		if (cigarStrings.containsKey(contig)) {

			HashMap<Integer, String> index = new HashMap<Integer, String>();
			ArrayList<String> unknLines = new ArrayList<String>();
			ArrayList<String> priorityLines = new ArrayList<String>();

			for (String line : cigarStrings.get(contig).split("\n")) {
				if (line.contains("UNKN"))
					unknLines.add(line);
				else
					priorityLines.add(line);
			}

			// Extract all coords
			priorityLines.addAll(unknLines);

			for (int i = 0; i < priorityLines.size(); i++) {
				// System.err.println(priorityLines.get(i));
				String[] lsplit = priorityLines.get(i).split("\\s");
				String[] ssplit = lsplit[0].split("\\.");
				String dScaf = ssplit[1].replace("chr", "");
				int dStart = Integer.parseInt(lsplit[1]);
				String[] ssSplit = lsplit[2].split("\\.");
				ssSplit[1] = ssSplit[1].replace("chr", "");
				ssSplit[0] = "";
				String sScaf = Utils.cleanJoin(ssSplit, ".");
				// System.err.println(sScaf);

				int sStart = Integer.parseInt(lsplit[3]);
				String cigar = lsplit[4];
				// int length = Integer.parseInt(lsplit[5]);
				String direction = lsplit[6];
				int offset = 0;
				String[] csplit = cigar.split(",");

				// Create a hash reference for each coordinate
				if (direction.equals("+")) {
					int dPos = dStart + 1;
					int sPos = sStart + 1;
					for (int j = 0; j < csplit.length; j++) {
						if (csplit[j].contains("M")) {
							offset = Integer.parseInt(csplit[j]
									.replace("M", ""));
							for (int k = 0; k < offset; k++) {
								if (!index.containsKey(sPos)
										|| index.get(sPos).equals("-1")) {
									index.put(sPos, dScaf + ":" + dPos + ":+");
								}
								dPos++;
								sPos++;
							}
						} else if (csplit[j].contains("I")) {
							offset = Integer.parseInt(csplit[j]
									.replace("I", ""));
							for (int k = 0; k < offset; k++) {
								if (!index.containsKey(sPos))
									index.put(sPos, "-1");
								sPos++;
							}
						} else if (csplit[j].contains("D")) {
							offset = Integer.parseInt(csplit[j]
									.replace("D", ""));
							for (int k = 0; k < offset; k++) {
								dPos++;
							}
						}
					}
				} else {
					int dPos = dStart + 1;
					int sPos = origRef.getSequenceLength(sScaf) - sStart;
					for (int j = 0; j < csplit.length; j++) {
						if (csplit[j].contains("M")) {
							offset = Integer.parseInt(csplit[j]
									.replace("M", ""));
							for (int k = 0; k < offset; k++) {
								if (!index.containsKey(sPos)
										|| index.get(sPos).equals("-1")) {
									index.put(sPos, dScaf + ":" + dPos + ":-");
								}
								dPos++;
								sPos--;
							}
						} else if (csplit[j].contains("I")) {
							offset = Integer.parseInt(csplit[j]
									.replace("I", ""));
							for (int k = 0; k < offset; k++) {
								if (!index.containsKey(sPos))
									index.put(sPos, "-1");
								sPos--;
							}
						} else if (csplit[j].contains("D")) {
							offset = Integer.parseInt(csplit[j]
									.replace("D", ""));
							for (int k = 0; k < offset; k++) {
								dPos++;
							}
						}
					}
				}
			}

			return index;

		}

		return new HashMap<Integer, String>();

	}
}
